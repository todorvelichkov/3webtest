<?php

/**
 * This class is configured from db.config.php
 */
class Model 
{
	private $_connection = null;

	function __construct($config)
	{
		$host = $config['host'];
		$database = $config['database'];
		$user = $config['user'];
		$password = $config['password'];
		
		// Set MySQL Connection
		$connection = mysql_connect($host, $user, $password) or die(mysql_error());
		mysql_select_db($database) or die(mysql_error());
		$this->_connection = $connection;
	}
	
	function getConnection() 
	{
		return $this->_connection;
	}
	
	/**
	 * Search for new RSSs and save them
	 */
	function saveNewRss($rssArray, $chanId)
	{

		$lastRss = $this->getLastRss();
		if (isset($lastRss)) {
			//save only new rss compared to the last one in DB
			$lastRssTime = strtotime($lastRss['rss_publication_date']);
			foreach ($rssArray as $rss ) {
				if ((strtotime($rss->pubDate) >= $lastRssTime)
					and ($rss->title <> $lastRss['rss_title'])) {
						$newRssArray[] = $rss;
				}
			}
		} else {
			//save all
			foreach ($rssArray as $rss) {
				//we do this to be able to do array_reverse() later
				$newRssArray[] = $rss;
			}
		}
		if (isset($newRssArray)) {
			$this->saveRssArray($newRssArray, $chanId);
		}
	}

	/**
	 * Insert into DB array of RSS records
	 */
	function saveRssArray($rssArray, $chanId)
	{
		$sql = array(); 
		foreach(array_reverse($rssArray) as $rss ) {
			$sql[] = '("'.mysql_real_escape_string($rss->title).'",
						"'.mysql_real_escape_string($rss->link).'",
						"'.mysql_real_escape_string(strip_tags($rss->description)).'",
						"'.mysql_real_escape_string($rss->enclosure['url']).'",
						"'.mysql_real_escape_string(strftime("%Y-%m-%d %H:%M:%S", strtotime($rss->pubDate))).'",
						"'.mysql_real_escape_string($rss->guid).'",
						"'.mysql_real_escape_string($chanId).'"'.')';
		}
		mysql_query('INSERT INTO 
			rss (
				rss_title, 
				rss_link, 
				rss_description,
				rss_jpeg_url, 
				rss_publication_date, 
				rss_guid,
				channel_id
			)
			VALUES '.implode(',', $sql)
		) or die(mysql_error());
	}
	
	/*
	 * Gets the RSS with the latest publication date
	 */
	function getLastRss()
	{
		$lastRss = mysql_query("SELECT * FROM rss 
						ORDER BY rss_publication_date 
						DESC LIMIT 1
					") or die(mysql_error());
		while($row = mysql_fetch_array($lastRss, MYSQL_ASSOC))	{
			$rss = $row;
		}
		if (isset($rss)) {
			return $rss;
		}
	}
	
	/*
	 * Gets the last 10 by publication date RSSs  
	 */
	function getLastTenRss()
	{
		$lastTenRss = mysql_query("SELECT * FROM rss 
						ORDER BY rss_publication_date
						DESC LIMIT 10
					") or die(mysql_error());
		return $this->convertToArray($lastTenRss);
	}
	
	/*
	 * Find all RSSs between two timestamps
	 */
	function getRssBetweenTwoDates($data)
	{
		$minDate = strftime("%Y-%m-%d %H:%M:%S", strtotime($data['start_date']));
		$maxDate = strftime("%Y-%m-%d %H:%M:%S", strtotime($data['end_date']));
		$rssRows = mysql_query("SELECT * FROM rss 
						WHERE rss_publication_date
						BETWEEN '$minDate' AND '$maxDate'
						ORDER BY rss_publication_date
						ASC
					") or die(mysql_error());
		return $this->convertToArray($rssRows);
	}
	
	/*
	 * Save new channel
	 */
	function saveChannel($channel)
	{
		$channel_title = $channel->title;
		$channel_link = $channel->link;
		$channel_description = $channel->description;
		$channel_language = $channel->language;

		// Insert information into table
		mysql_query("INSERT INTO 
			channels (
				channel_title, 
				channel_link, 
				channel_description,
				channel_language
			) 
			VALUES (
				'$channel_title', 
				'$channel_link', 
				'$channel_description', 
				'$channel_language'
			)
		")
		or die(mysql_error());
	}
	
	/*
	 * Finds channel by title
	 */
	function findChannel($channel_title)
	{
		$channel = mysql_query("SELECT * FROM channels 
						WHERE channel_title = '$channel_title'
					") or die(mysql_error());
		while($row = mysql_fetch_array($channel, MYSQL_ASSOC))	{
			$chan = $row;
		}
		if (isset($chan)) {
			return $chan;
		}
	}
	
	/*
	 * Returns channel id
	 */
	function getChannelId($channel_title)
	{
		$channel = $this->findChannel($channel_title);
		if ($channel) {
			return $channel['channel_id'];			
		}
	}
	
	/*
	 * Convert MySql result set to associative array
	 */
	function convertToArray($rows) 
	{
		while($row = mysql_fetch_array($rows, MYSQL_ASSOC))	{
			$arrayResult[] = $row;
		}
		if (isset($arrayResult)) {
			return $arrayResult;
		}
	}
}