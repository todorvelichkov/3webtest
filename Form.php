<?php

/**
 * Form Class
 */
class Form
{
	/*
	 * Render the search form
	 */
	function render() 
	{
		require_once 'FormView.phtml';
	}
	
	/*
	 * Validators
	 */
	function isValid() 
	{
		$validators = Array();
		$data = $this->getPost();
		
		$validators['start_date'] = $this->validate($data['start_date'],'alnum',30, array('/',' ','-',':'));
		$validators['end_date'] = $this->validate($data['end_date'],'alnum',30, array('/',' ','-',':'));

		$err = true;
		foreach ($validators as $key=>$val) {
			if ($val == false) {
				echo 'Inccorect: ',$key,'<br>';
				$err = false;
			}
		}
		return $err;
	}

	/*
	 * Get the data from the $_POST
	 */
	function getPost() 
	{
		$post = Array();
		$post['start_date'] = (isset($_POST['start_date']) ? $_POST['start_date'] : '');
		$post['end_date'] = (isset($_POST['end_date']) ? $_POST['end_date'] : '');
		return $post;		
	}
	
	/*
	 *	Napisano ot Martin Psinas, 10x Martin.
     *
	 * validate user input
	 * 	$input: variable to be validated
	 * 	$type: nofilter, alpha, numeric, alnum, email, url, ip
	 * 	$len: maximum length
	 *	$chars: array of any non alpha-numeric characters to allow
	 *	example use:
	 *		$phone = isset($_POST['phone']) && validate($_POST['phone'], 'numeric', 20, array('(',')','-')) ? $_POST['phone'] : null;
	 *		$email_addr = isset($_POST['email_addr']) && validate($_POST['email_addr'], 'email', 255) ? $_POST['email_addr'] : null;
	 *		$msg = isset($_POST['msg']) && validate($_POST['msg'], 'nofilter') ? $_POST['msg'] : null;
	 */
	function validate($input, $type, $len = null, $chars = null) {
		$tmp = str_replace(' ', '', $input);
		if(!empty($tmp)) {
			if(isset($len)) {
				if(strlen($input) > $len) {
					return FALSE;
				}
			}
			if(isset($chars)) {
				$input = str_replace($chars, '', $input);
			}
			$input = str_replace(' ', '', $input);

			switch($type) {
				case 'alpha':
					if(!ctype_alpha($input)) {
						return FALSE;
					}
				break;

				case 'numeric':
					if(!ctype_digit($input)) {
						return FALSE;
					}
				break;

				case 'alnum':
					if(!ctype_alnum($input)) {
						return FALSE;
					}
				break;

				case 'email':
					if(!filter_var($input, FILTER_VALIDATE_EMAIL)) {
						return FALSE;
					}
				break;

				case 'url':
					if(!filter_var($input, FILTER_VALIDATE_URL)) {
						return FALSE;
					}
				break;

				case 'ip':
					if(!filter_var($input, FILTER_VALIDATE_IP)) {
						return FALSE;
					}
				break;

				case 'nofilter':
					return TRUE;
				break;
			}
			return TRUE;
		} else {
			return FALSE;
		}
	}
}