<?php
date_default_timezone_set("Europe/Helsinki");
require_once '/../Model.php';
require_once '/../Form.php';

$model = new Model(require '/../db.config.php');
$form = new Form();

if (isset($_POST['submit'])) {
	$form->getPost();
	if ($form->isValid()) {
		$data = $form->getPost();
		$rssArray = $model->getRssBetweenTwoDates($data);
	}
}
if (!isset($rssArray))
{
	//by default we just show last 10 RSSs
	$rssArray = $model->getLastTenRss();
}
require_once '/../layout.phtml';
?>
