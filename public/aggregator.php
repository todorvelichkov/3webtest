<?php
require_once '/../Model.php';
date_default_timezone_set("Europe/Helsinki");

$url = "http://rss.dnes.bg/c/33162/f/539026/index.rss?today"; // url of dnes.bg news
$rss = simplexml_load_file($url); //loading the document as Object

if($rss) {

	$model = new Model(require '/../db.config.php');

	$channel = $rss->channel;
	$chanTitle = $channel->title;

	if ($model->findChannel($chanTitle)) {
		$chanId = $model->getChannelId($chanTitle);
	} else {
		//save new channel
		$model->saveChannel($channel);
		//get the id
		$chanId = $model->getChannelId($chanTitle);
	}
	
	$items = $channel->item;
	$model->saveNewRss($items, $chanId);
}

header("location: index.php"); 

