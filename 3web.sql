-- phpMyAdmin SQL Dump
-- version 3.3.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 19, 2013 at 08:17 PM
-- Server version: 5.1.50
-- PHP Version: 5.3.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `3web`
--

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE IF NOT EXISTS `channels` (
  `channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `channel_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `channel_language` varchar(31) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`channel_id`, `channel_title`, `channel_link`, `channel_description`, `channel_language`) VALUES
(1, 'DNES.BG: Новините днес', 'http://www.dnes.bg/', 'News Today', 'bg');

-- --------------------------------------------------------

--
-- Table structure for table `rss`
--

CREATE TABLE IF NOT EXISTS `rss` (
  `rss_id` int(11) NOT NULL AUTO_INCREMENT,
  `rss_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rss_link` varchar(511) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rss_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rss_jpeg_url` varchar(511) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rss_publication_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rss_guid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL COMMENT 'FK to channels',
  PRIMARY KEY (`rss_id`),
  KEY `rss's channel` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=83 ;

--
-- Dumping data for table `rss`
--

INSERT INTO `rss` (`rss_id`, `rss_title`, `rss_link`, `rss_description`, `rss_jpeg_url`, `rss_publication_date`, `rss_guid`, `channel_id`) VALUES
(58, 'За разликите между диабет тип I и II', 'http://rss.dnes.bg/c/33162/f/539026/s/28b94345/l/0L0Sdnes0Bbg0Chealth0C20A130C0A20C190Cza0Erazlikite0Emejdu0Ediabet0Etip0Ei0Ei0Eii0B180A461/story01.htm', 'Захарният диабет протича в две основни форми', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b94345/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A4610Etop20Bjpg/0000180461-top2.jpg', '2013-02-19 15:41:00', 'http://www.dnes.bg/health/2013/02/19/za-razlikite-mejdu-diabet-tip-i-i-ii.180461', 1),
(59, 'Audi принуди Qoros да преименува първата си кола', 'http://rss.dnes.bg/c/33162/f/539026/s/28b94342/l/0L0Sdnes0Bbg0Ccars0C20A130C0A20C190Caudi0Eprinudi0Eqoros0Eda0Epreimenuva0Epyrvata0Esi0Ekola0B180A583/story01.htm', 'Новото име обаче вероятно ще предизвика конфликт BMW', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b94342/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5830Etop20Bjpg/0000180583-top2.jpg', '2013-02-19 15:50:00', 'http://www.dnes.bg/cars/2013/02/19/audi-prinudi-qoros-da-preimenuva-pyrvata-si-kola.180583', 1),
(60, 'Не бъдете сигурни, че Борисов няма да хвърли оставка!', 'http://rss.dnes.bg/c/33162/f/539026/s/28b99923/l/0L0Sdnes0Bbg0Cpolitika0C20A130C0A20C190Cne0Ebydete0Esigurni0Eche0Eborisov0Eniama0Eda0Ehvyrli0Eostavka0B180A586/story01.htm', 'Явно на ГЕРБ им се иска Цветанов да им организира вота, каза Местан', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b99923/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5860Etop20Bjpg/0000180586-top2.jpg', '2013-02-19 15:53:00', 'http://www.dnes.bg/politika/2013/02/19/ne-bydete-sigurni-che-borisov-niama-da-hvyrli-ostavka.180586', 1),
(61, 'Ибра: Един ден може и да играя в Байерн Мюнхен', 'http://rss.dnes.bg/c/33162/f/539026/s/28b99922/l/0L0Sdnes0Bbg0Cindex0C20A130C0A20C190Cibra0Eedin0Eden0Emoje0Ei0Eda0Eigraia0Ev0Ebaiern0Emiunhen0B180A568/story01.htm', '"Този клуб е в Топ 5 на света, Бундеслигата е интересно първенство", отсече асът на ПСЖ', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b99922/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5680Etop20Bjpg/0000180568-top2.jpg', '2013-02-19 15:57:00', 'http://www.dnes.bg/index/2013/02/19/ibra-edin-den-moje-i-da-igraia-v-baiern-miunhen.180568', 1),
(62, 'Кейт се появи за първи път с бебешко коремче', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9afb2/l/0L0Sdnes0Bbg0Cmish0Emash0C20A130C0A20C190Ckeit0Ese0Epoiavi0Eza0Epyrvi0Epyt0Es0Ebebeshko0Ekoremche0B180A579/story01.htm', 'Тя посети благотворителен жилищен център, на който е патрон', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9afb2/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5790Etop20Bjpg/0000180579-top2.jpg', '2013-02-19 16:07:00', 'http://www.dnes.bg/mish-mash/2013/02/19/keit-se-poiavi-za-pyrvi-pyt-s-bebeshko-koremche.180579', 1),
(63, 'Вместо да хвърлят домати, да се направи национален дебат', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9afaf/l/0L0Sdnes0Bbg0Cpolitika0C20A130C0A20C190Cvmesto0Eda0Ehvyrliat0Edomati0Eda0Ese0Enapravi0Enacionalen0Edebat0B180A577/story01.htm', 'Президентът иска международни проверки в енергетиката', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9afaf/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5770Etop20Bjpg/0000180577-top2.jpg', '2013-02-19 16:16:00', 'http://www.dnes.bg/politika/2013/02/19/vmesto-da-hvyrliat-domati-da-se-napravi-nacionalen-debat.180577', 1),
(64, 'Навсякъде революцията е изяла децата си!', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9afab/l/0L0Sdnes0Bbg0Cpolitika0C20A130C0A20C190Cnavsiakyde0Erevoliuciiata0Ee0Eiziala0Edecata0Esi0B180A590A/story01.htm', 'Неделя е почивен ден, играх футбол, отчете премиерът', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9afab/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A590A0Etop20Bjpg/0000180590-top2.jpg', '2013-02-19 16:17:00', 'http://www.dnes.bg/politika/2013/02/19/navsiakyde-revoliuciiata-e-iziala-decata-si.180590', 1),
(65, 'В Брюксел откраднаха диаманти за €350 млн.', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9b419/l/0L0Sdnes0Bbg0Cworld0C20A130C0A20C190Cv0Ebriuksel0Eotkradnaha0Ediamanti0Eza0E350A0Emln0B180A526/story01.htm', 'При нападение е отмъкнат контейнер със скъпоценни камъни и злато', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9b419/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5260Etop20Bjpg/0000180526-top2.jpg', '2013-02-19 16:35:00', 'http://www.dnes.bg/world/2013/02/19/v-briuksel-otkradnaha-diamanti-za-350-mln.180526', 1),
(66, 'Сашо Диков сменя Карбовски по Нова тв', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9a76c/l/0L0Sdnes0Bbg0Ctelevizia0C20A130C0A20C190Csasho0Edikov0Esmenia0Ekarbovski0Epo0Enova0Etv0B180A588/story01.htm', 'От 2 март започва тричасовото предаване "Дикoff"', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9a76c/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5880Etop20Bjpg/0000180588-top2.jpg', '2013-02-19 16:43:00', 'http://www.dnes.bg/televizia/2013/02/19/sasho-dikov-smenia-karbovski-po-nova-tv.180588', 1),
(67, 'БСП, "Атака" и ДПС бойкотираха икономическата комисия', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9a769/l/0L0Sdnes0Bbg0Cbulgaria0C20A130C0A20C190Cbsp0Eataka0Ei0Edps0Eboikotiraha0Eikonomicheskata0Ekomisiia0B180A585/story01.htm', 'Няма да спасявам Борисов, скочи Румен Овчаров', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9a769/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5850Etop20Bjpg/0000180585-top2.jpg', '2013-02-19 16:46:00', 'http://www.dnes.bg/bulgaria/2013/02/19/bsp-ataka-i-dps-boikotiraha-ikonomicheskata-komisiia.180585', 1),
(68, 'Джон Тери: Надявам се Лампард да остане в Челси', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9bceb/l/0L0Sdnes0Bbg0Csport0C20A130C0A20C190Cdjon0Eteri0Enadiavam0Ese0Elampard0Eda0Eostane0Ev0Echelsi0B180A569/story01.htm', '"Той винаги е бил вдъхновение за всички в нашия отбор", допълни бранителят', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9bceb/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5690Etop20Bjpg/0000180569-top2.jpg', '2013-02-19 16:58:00', 'http://www.dnes.bg/sport/2013/02/19/djon-teri-nadiavam-se-lampard-da-ostane-v-chelsi.180569', 1),
(69, 'Това е новата Skoda Octavia Combi', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9bcea/l/0L0Sdnes0Bbg0Ccars0C20A130C0A20C190Ctova0Ee0Enovata0Eskoda0Eoctavia0Ecombi0B180A580A/story01.htm', 'Чехите разпространиха първите официални снимки и информация', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9bcea/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A580A0Etop20Bjpg/0000180580-top2.jpg', '2013-02-19 17:01:00', 'http://www.dnes.bg/cars/2013/02/19/tova-e-novata-skoda-octavia-combi.180580', 1),
(70, 'Няма какво повече да правим в този парламент!', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9bce8/l/0L0Sdnes0Bbg0Cpolitika0C20A130C0A20C190Cniama0Ekakvo0Epoveche0Eda0Epravim0Ev0Etozi0Eparlament0B180A598/story01.htm', 'БСП обмисля дали да влиза повече в НС', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9bce8/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5980Etop20Bjpg/0000180598-top2.jpg', '2013-02-19 17:05:00', 'http://www.dnes.bg/politika/2013/02/19/niama-kakvo-poveche-da-pravim-v-tozi-parlament.180598', 1),
(71, 'Официално: Ще има видеотехнологии на Мондиал 2014', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9bce7/l/0L0Sdnes0Bbg0Csport0C20A130C0A20C190Coficialno0Eshte0Eima0Evideotehnologii0Ena0Emondial0E20A140B180A596/story01.htm', 'До момента ФИФА одобри "ГолРеф" и "ХоукАй", през април избира само една система', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9bce7/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5960Etop20Bjpg/0000180596-top2.jpg', '2013-02-19 17:10:00', 'http://www.dnes.bg/sport/2013/02/19/oficialno-shte-ima-videotehnologii-na-mondial-2014.180596', 1),
(72, 'Пхенян заплаши Южна Корея с "окончателно унищожение"', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9e5c5/l/0L0Sdnes0Bbg0Cworld0C20A130C0A20C190Cphenian0Ezaplashi0Eiujna0Ekoreia0Es0Eokonchatelno0Eunishtojenie0B180A592/story01.htm', 'Новородено кученце не се страхува от тигъра, заяви дипломат от КНДР в ООН', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9e5c5/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5920Etop20Bjpg/0000180592-top2.jpg', '2013-02-19 17:15:00', 'http://www.dnes.bg/world/2013/02/19/phenian-zaplashi-iujna-koreia-s-okonchatelno-unishtojenie.180592', 1),
(73, 'Американка роди две двойки еднояйчни близнаци в един и същ ден', 'http://rss.dnes.bg/c/33162/f/539026/s/28b9e5c4/l/0L0Sdnes0Bbg0Cakoshtete0Evqrvaite0C20A130C0A20C190Camerikanka0Erodi0Edve0Edvoiki0Eednoiaichni0Ebliznaci0Ev0Eedin0Ei0Esysht0Eden0B180A554/story01.htm', 'Шансът да се случи подобно нещо е едно на 70 милиона', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28b9e5c4/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5540Etop20Bjpg/0000180554-top2.jpg', '2013-02-19 17:19:00', 'http://www.dnes.bg/akoshtete-vqrvaite/2013/02/19/amerikanka-rodi-dve-dvoiki-ednoiaichni-bliznaci-v-edin-i-sysht-den.180554', 1),
(74, 'Денят в снимки', 'http://rss.dnes.bg/c/33162/f/539026/s/28ba438a/l/0L0Sdnes0Bbg0Cmish0Emash0C20A130C0A20C190Cdeniat0Ev0Esnimki0B180A594/story01.htm', 'Мис Свят 2012 и сняг в Китай', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28ba438a/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5940Etop20Bjpg/0000180594-top2.jpg', '2013-02-19 17:43:00', 'http://www.dnes.bg/mish-mash/2013/02/19/deniat-v-snimki.180594', 1),
(75, 'МО вади "Пустинни котки" за честванията на Левски', 'http://rss.dnes.bg/c/33162/f/539026/s/28ba4387/l/0L0Sdnes0Bbg0Cobshtestvo0C20A130C0A20C190Cmo0Evadi0Epustinni0Ekotki0Eza0Echestvaniiata0Ena0Elevski0B180A60A5/story01.htm', 'В София ще има и митинг на "Атака" и протест за високите сметки за ток', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28ba4387/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A60A50Etop20Bjpg/0000180605-top2.jpg', '2013-02-19 17:55:00', 'http://www.dnes.bg/obshtestvo/2013/02/19/mo-vadi-pustinni-kotki-za-chestvaniiata-na-levski.180605', 1),
(76, 'Скандал с протестираща, свързаха я с ГЕРБ', 'http://rss.dnes.bg/c/33162/f/539026/s/28ba7bda/l/0L0Sdnes0Bbg0Cobshtestvo0C20A130C0A20C190Cskandal0Es0Eprotestirashta0Esvyrzaha0Eia0Es0Egerb0B180A599/story01.htm', 'Децата ми не са назначени по политическа линия, оправда се Пеловска', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28ba7bda/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5990Etop20Bjpg/0000180599-top2.jpg', '2013-02-19 18:05:00', 'http://www.dnes.bg/obshtestvo/2013/02/19/skandal-s-protestirashta-svyrzaha-ia-s-gerb.180599', 1),
(77, 'Mercedes-Benz направи най-бързата електрическа лодка в света', 'http://rss.dnes.bg/c/33162/f/539026/s/28ba6f19/l/0L0Sdnes0Bbg0Ccars0C20A130C0A20C190Cmercedes0Ebenz0Enapravi0Enai0Ebyrzata0Eelektricheska0Elodka0Ev0Esveta0B180A582/story01.htm', 'Мощността на катера AMG Electric Drive Concept е 2551 к.с.', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28ba6f19/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5820Etop20Bjpg/0000180582-top2.jpg', '2013-02-19 18:18:00', 'http://www.dnes.bg/cars/2013/02/19/mercedes-benz-napravi-nai-byrzata-elektricheska-lodka-v-sveta.180582', 1),
(78, 'Докато България протестира, Борисов си почива!', 'http://rss.dnes.bg/c/33162/f/539026/s/28bb6e1a/l/0L0Sdnes0Bbg0Cpolitika0C20A130C0A20C190Cdokato0Ebylgariia0Eprotestira0Eborisov0Esi0Epochiva0B180A578/story01.htm', 'Премиерът е слаб и уплашен, смятат хората на Кунева', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28bb6e1a/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A5780Etop20Bjpg/0000180578-top2.jpg', '2013-02-19 18:37:00', 'http://www.dnes.bg/politika/2013/02/19/dokato-bylgariia-protestira-borisov-si-pochiva.180578', 1),
(79, 'Спряха за три дни борсовата търговия с ЕРП-тата', 'http://rss.dnes.bg/c/33162/f/539026/s/28bb97e7/l/0L0Sdnes0Bbg0Cbusiness0C20A130C0A20C190Cspriaha0Eza0Etri0Edni0Eborsovata0Etyrgoviia0Es0Eerp0Etata0B180A60A8/story01.htm', 'Книжата на ЧЕЗ не се търгуваха в по-голямата част от днешната сесия', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28bb97e7/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A60A80Etop20Bjpg/0000180608-top2.jpg', '2013-02-19 18:52:00', 'http://www.dnes.bg/business/2013/02/19/spriaha-za-tri-dni-borsovata-tyrgoviia-s-erp-tata.180608', 1),
(80, 'Обвинения за трима, регистрирали незаконно коли', 'http://rss.dnes.bg/c/33162/f/539026/s/28bb771c/l/0L0Sdnes0Bbg0Ctemida0C20A130C0A20C190Cobvineniia0Eza0Etrima0Eregistrirali0Enezakonno0Ekoli0B180A60A2/story01.htm', 'Те подправяли документите на автомобилите', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28bb771c/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A60A20Etop20Bjpg/0000180602-top2.jpg', '2013-02-19 19:07:00', 'http://www.dnes.bg/temida/2013/02/19/obvineniia-za-trima-registrirali-nezakonno-koli.180602', 1),
(81, 'Чехия се обезпокои за отнетия лиценз на CEZ', 'http://rss.dnes.bg/c/33162/f/539026/s/28bb7bb6/l/0L0Sdnes0Bbg0Cworld0C20A130C0A20C190Cchehiia0Ese0Eobezpokoi0Eza0Eotnetiia0Elicenz0Ena0Ecez0B180A612/story01.htm', 'Ситуацията е много тревожна, заяви чешки министър', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28bb7bb6/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A6120Etop20Bjpg/0000180612-top2.jpg', '2013-02-19 19:25:00', 'http://www.dnes.bg/world/2013/02/19/chehiia-se-obezpokoi-za-otnetiia-licenz-na-cez.180612', 1),
(82, 'Хакнаха сайта на ЧЕЗ', 'http://rss.dnes.bg/c/33162/f/539026/s/28bba73a/l/0L0Sdnes0Bbg0Cobshtestvo0C20A130C0A20C190Chaknaha0Esaita0Ena0Echez0B180A611/story01.htm', 'От дружеството обясняват, че са жертва на масирана атака', 'http://rss.dnes.bg/c/33162/f/539026/e/1/s/28bba73a/l/0L850B140B280B1640Cd0Cimages0Cphotos0C0A180A0C0A0A0A0A180A6110Etop20Bjpg/0000180611-top2.jpg', '2013-02-19 19:45:00', 'http://www.dnes.bg/obshtestvo/2013/02/19/haknaha-saita-na-chez.180611', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rss`
--
ALTER TABLE `rss`
  ADD CONSTRAINT `rss's channel` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`channel_id`) ON DELETE SET NULL ON UPDATE CASCADE;
